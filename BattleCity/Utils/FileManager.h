#pragma once

#include <Shlwapi.h>
#include <iostream>

class FileManager
{
private:
	FileManager();
public:
	static char* GetThisPath(char* dest, size_t destSize);
	static const char* GetFullPathToFile(std::string relevantPath, std::string& fullPath);
};
