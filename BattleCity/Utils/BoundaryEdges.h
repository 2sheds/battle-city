#pragma once

#include "Vec2D.h"
#include "Line2D.h"

enum EdgeType
{
	BOTTOM_EDGE = 0,
	TOP_EDGE,
	LEFT_EDGE,
	RIGHT_EDGE,
	NUM_EDGES
};

struct BoundaryEdge
{
	Line2D edge;
	Vec2D normal;
};

