#pragma once
#include "Vec2D.h"
#include <cmath>
#include <iostream>
#include <random>
#include <vector>
#include <cstring>

static const float EPSILON = 0.0001f;

static const Vec2D UP_DIR = { 0, -1 };
static const Vec2D DOWN_DIR = { 0, 1 };
static const Vec2D LEFT_DIR = { -1, 0 };
static const Vec2D RIGHT_DIR = { 1, 0 };

static const int LVL_GRID_WIDTH = 13;
static const int LVL_GRID_HEIGHT = 13;
static const int CELL_SIDE_SIZE = 40;
static const int BATTLEFIELD_WIDTH = LVL_GRID_WIDTH * CELL_SIDE_SIZE;
static const int BATTLEFIELD_HEIGHT = LVL_GRID_HEIGHT * CELL_SIDE_SIZE;
static const int MIN_RESOLUTION_WIDTH = BATTLEFIELD_WIDTH + 80;
static const int MIN_RESOLUTION_HEIGHT = BATTLEFIELD_HEIGHT + 50;

enum Direction
{
	TOP = 0,
	DOWN,
	LEFT,
	RIGHT,
	NUM_DIRECTION
};


class UtilsLibrary
{
public:
	static bool IsEqual(float x, float y)
	{
		return fabsf(x - y) < EPSILON;
	}

	static float MillisecondsToSeconds(unsigned int milliseconds)
	{
		return static_cast<float>(milliseconds) / 1000.0f;
	}

	static int GetRandomNumber(int max, int min = 0)
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> distrib(0, max);
		return distrib(gen);
	}

	static bool ParseMainArgsIntoResolution(int argc, char* argv[], int& width, int& height)
	{
		char* charWidth = nullptr;
		char* charHeight = nullptr;

		for (int i = 1; i < argc; i++) {

			if (strcmp(argv[i], "-window") == 0) {
				charWidth = strtok_s(argv[i + 1], "x", &charHeight);
				if (charWidth)
				{
					width = atoi(charWidth);
				}
				if (charHeight)
				{
					height = atoi(charHeight);
				}
			}
		}

		if (width < MIN_RESOLUTION_WIDTH || height < MIN_RESOLUTION_HEIGHT)
		{
			return false;
		}
		return true;
	}
};

