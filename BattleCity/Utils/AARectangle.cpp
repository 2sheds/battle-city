#include "AARectangle.h"
#include "Utils.h"

#include <cmath>

void AARectangle::Init(const int topLeftX, const int topLeftY, unsigned int width, unsigned int height)
{
	mPoints.push_back(Vec2D(topLeftX, topLeftY));
	mPoints.push_back(Vec2D(topLeftX + width - 1, topLeftY + height - 1));
}

float AARectangle::GetWidth() const {
	return GetBottomRightPoint().GetX() - GetTopLeftPoint().GetX() + 1;
}

float AARectangle::GetHeight() const {
	return GetBottomRightPoint().GetY() - GetTopLeftPoint().GetY() + 1;
}

void AARectangle::MoveTo(const Vec2D& position) {
	float width = GetWidth();
	float height = GetHeight();

	SetTopLeftPoint(position);
	SetBottomRightPoint(Vec2D(position.GetX() + width - 1,
		position.GetY() + height - 1));

}

void AARectangle::MoveBy(const Vec2D& deltaOffset)
{
	for (Vec2D& point : mPoints)
	{
		point = point + deltaOffset;
	}
}

Vec2D AARectangle::GetCenterPoint() const {
	return Vec2D(GetTopLeftPoint().GetX() + GetWidth() / 2.0f,
		GetTopLeftPoint().GetY() + GetHeight() / 2.0f);
}

bool AARectangle::Intersects(const AARectangle& otherRect) const
{
	return !(otherRect.GetBottomRightPoint().GetX() < GetTopLeftPoint().GetX() ||
		otherRect.GetTopLeftPoint().GetX() > GetBottomRightPoint().GetX() ||
		otherRect.GetBottomRightPoint().GetY() < GetTopLeftPoint().GetY() ||
		otherRect.GetTopLeftPoint().GetY() > GetBottomRightPoint().GetY());
}

bool AARectangle::ContainsPoint(const Vec2D& point) const
{
	bool withinX = point.GetX() >= GetTopLeftPoint().GetX() &&

		point.GetX() <= GetBottomRightPoint().GetX();
	bool withinY = point.GetY() >= GetTopLeftPoint().GetY() &&
		point.GetY() <= GetBottomRightPoint().GetY();
	return withinX && withinY;
}

std::vector<Vec2D> AARectangle::GetPoints() const
{
	std::vector<Vec2D> points;
	points.push_back(mPoints[0]); // ��� ������ ����� � ��� ��� ����
	points.push_back(Vec2D(mPoints[1].GetX(), mPoints[0].GetY()));
	points.push_back(mPoints[1]); // bottom right
	points.push_back(Vec2D(mPoints[0].GetX(), mPoints[1].GetY()));

	return points;
}

