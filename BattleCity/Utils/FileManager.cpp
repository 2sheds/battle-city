#include "FileManager.h"
//#include <Shlwapi.h>

#pragma comment(lib, "shlwapi.lib")

char* FileManager::GetThisPath(char* dest, size_t destSize)
{
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);

	return dest;
}

const char* FileManager::GetFullPathToFile(std::string relevantPath, std::string& fullPath)
{
	char dest[MAX_PATH];
	fullPath = GetThisPath(dest, MAX_PATH) + relevantPath;
	
	return &fullPath[0];
}

