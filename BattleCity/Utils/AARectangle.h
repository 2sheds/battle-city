#pragma once 

#include "Vec2D.h"
#include <vector>

class AARectangle 
{
public:

	void Init(const int topLeftX, const int topLeftY, unsigned int width, unsigned int height);

	inline void SetTopLeftPoint(const Vec2D& topLeft) {
		mPoints[0] = topLeft;
	}
	inline void SetBottomRightPoint(const Vec2D& bottomRight) {
		mPoints[1] = bottomRight;
	}
	
	inline Vec2D GetTopLeftPoint() const { return mPoints[0]; }
	inline Vec2D GetBottomRightPoint() const {return mPoints[1]; }

	float GetWidth() const;
	float GetHeight() const;

	virtual void MoveTo(const Vec2D& position);
	virtual void MoveBy(const Vec2D& deltaOffset);

	Vec2D GetCenterPoint() const;

	bool Intersects(const AARectangle& otherRect) const;
	bool ContainsPoint(const Vec2D& point) const;

	std::vector<Vec2D> GetPoints() const;

	inline void ClearPoints() { mPoints.clear(); };

private:
	std::vector<Vec2D> mPoints;
};
