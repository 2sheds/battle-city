#pragma once

#include "SpriteObject.h"

class UserInterface
{
public:
	void Init();
	void Update();
private:
	Sprite* mEnemyTankIcon;
	Sprite* mPlayerLivesIcon;
};

