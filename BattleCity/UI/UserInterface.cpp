#include "UserInterface.h"
#include "SpriteLibrary.h"
#include "FileManager.h"
#include "GameLevel.h"
#include "Vec2D.h"

#include <string>

void UserInterface::Init()
{
	std::string fullPath;

	mEnemyTankIcon = SpriteLibrary::GetUISprites(UIsprites::TANK_ENEMY_ICON);
	mPlayerLivesIcon = SpriteLibrary::GetUISprites(UIsprites::PLAYER_LIVES_ICON);
}

void UserInterface::Update()
{
	int enemyTanksToKill = GameLevel::Singleton().mEnemyTanksToKill;
	int enemyTanksPerColumn = GameLevel::Singleton().ENEMY_TANKS_LIMIT / 2;
	int cellOffset = 3 + 32;
	Vec2D enemyTanksTopLeftCorner(BATTLEFIELD_WIDTH + 8, 12);

	for (int i = 0; i < enemyTanksToKill; i++)
	{
		drawSprite(mEnemyTankIcon, enemyTanksTopLeftCorner.GetX() +
					(i / enemyTanksPerColumn) * cellOffset,
					enemyTanksTopLeftCorner.GetY() + (i % enemyTanksPerColumn) * cellOffset );
	}

	int playerTankLives = GameLevel::Singleton().mPlayerTank.mCurrentLives;
	Vec2D playerTankTopLeftCorner(15, BATTLEFIELD_HEIGHT + 8);

	for (int i = 0; i < playerTankLives; i++)
	{
		drawSprite(mPlayerLivesIcon, playerTankTopLeftCorner.GetX() + i * (cellOffset),
			playerTankTopLeftCorner.GetY());
	}
}
