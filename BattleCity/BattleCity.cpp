#include "BattleCity.h"
#include "SpriteLibrary.h"
#include "FileManager.h"
#include <iostream>


GameState BattleCity::mGameState = GameState::MAIN_MENU;

BattleCity::~BattleCity()
{
	delete mSpriteStart;
	delete mSpriteLost;
	delete mSpriteWin;
}

void BattleCity::PreInit(int& width, int& height, bool& fullscreen)
{
	width = mDefaultScreenWidth;
	height = mDefaultScreenHeight;
	fullscreen = false;
}

bool BattleCity::Init()
{
	SpriteLibrary::Init();
	ResetGame();
	mLevelBoundary.Init(BATTLEFIELD_WIDTH, BATTLEFIELD_HEIGHT);
	std::string fullPath;

	mSpriteStart = SpriteLibrary::GetUISprites(UIsprites::MAIN_MENU);
	mSpriteLost = SpriteLibrary::GetUISprites(UIsprites::GAME_OVER_LOST_MENU);
	mSpriteWin = SpriteLibrary::GetUISprites(UIsprites::GAME_OVER_WIN_MENU);

	mLastTick = getTickCount();
	mCurrentTick = mLastTick;
	mIsExited = false;

	return true;
}

void BattleCity::Close()
{
	mIsExited = true;
}

bool BattleCity::Tick() 
{
	UpdateDeltaTime();

	if (mGameState == GameState::IN_GAME)
	{
		if (GameLevel::Singleton().mPlayerTank.mIsAlife)
		{
			GameLevel::Singleton().mPlayerTank.Update(mDeltaTime);
			GameLevel::Singleton().Update(mBullet, mDeltaTime);
		}
	}

	Draw();

	return mIsExited;
}

void BattleCity::UpdateDeltaTime()
{
	mCurrentTick = getTickCount();

	mDeltaTime = mCurrentTick - mLastTick;

	mLastTick = mCurrentTick;
}

void BattleCity::onKeyPressed(FRKey k)
{
	switch (mGameState)
	{
	case GameState::MAIN_MENU:
		if (k == FRKey::UP)
		{
			mGameState = GameState::IN_GAME;
		}
		else if (k == FRKey::DOWN)
		{
			mIsExited = true;
		}
		break;

	case GameState::IN_GAME:
		OnKeyPressedInGameState(k);
		break;

	case GameState::GAME_OVER_LOST:
	case GameState::GAME_OVER_WIN:
		if (k == FRKey::LEFT)
		{
			ResetGame();
			mGameState = GameState::IN_GAME;
		}
		else if (k == FRKey::RIGHT)
		{
			ResetGame();
			mGameState = GameState::MAIN_MENU;
		}
		break;
	}
}

void BattleCity::OnKeyPressedInGameState(FRKey k)
{
	switch (k)
	{
	case FRKey::RIGHT:
		GameLevel::Singleton().mPlayerTank.SetMovementDirection(Direction::RIGHT);
		break;
	case FRKey::LEFT:
		GameLevel::Singleton().mPlayerTank.SetMovementDirection(Direction::LEFT);
		break;
	case FRKey::DOWN:
		GameLevel::Singleton().mPlayerTank.SetMovementDirection(Direction::DOWN);
		break;
	case FRKey::UP:
		GameLevel::Singleton().mPlayerTank.SetMovementDirection(Direction::TOP);
		break;
	default:
		break;
	}
}

void BattleCity::onKeyReleased(FRKey k)
{
	if (mGameState == GameState::IN_GAME)
	{
		switch (k)
		{
		case FRKey::RIGHT:
			GameLevel::Singleton().mPlayerTank.UnsetMovementDirection(RIGHT_DIR);
			break;
		case FRKey::LEFT:
			GameLevel::Singleton().mPlayerTank.UnsetMovementDirection(LEFT_DIR);
			break;
		case FRKey::DOWN:
			GameLevel::Singleton().mPlayerTank.UnsetMovementDirection(DOWN_DIR);
			break;
		case FRKey::UP:
			GameLevel::Singleton().mPlayerTank.UnsetMovementDirection(UP_DIR);
			break;
		default:
			break;
		}
	}
}

void BattleCity::Draw()
{
	switch (mGameState)
	{
	case GameState::MAIN_MENU:
		drawSprite(mSpriteStart, 0, 0);
		break;
	case GameState::IN_GAME:
	{
		GameLevel::Singleton().Draw();
		GameLevel::Singleton().mPlayerTank.Draw();
		SpriteLibrary::Draw();
	}
	break;
	case GameState::GAME_OVER_LOST:
		drawSprite(mSpriteLost, 0, 0);
		break;
	case GameState::GAME_OVER_WIN:
		drawSprite(mSpriteWin, 0, 0);
		break;
	}
}

void BattleCity::onMouseMove(int x, int y, int xrelative, int yrelative)
{}

void BattleCity::onMouseButtonClick(FRMouseButton button, bool isReleased)
{
	if (mGameState == GameState::IN_GAME && !isReleased)
	{
		if (button == FRMouseButton::LEFT && !GameLevel::Singleton().mPlayerTank.mBullet.mWasLaunched)
		{
			GameLevel::Singleton().mPlayerTank.Shot();
		}
	}
}

const char* BattleCity::GetTitle()
{
	return "Battle City";
}

void BattleCity::ResetGame()
{
	mGameState = GameState::MAIN_MENU;
	mDeltaTime = 0;

	GameLevel::Singleton().mPlayerTank.Init();
	GameLevel::Singleton().Init();
	GameLevel::Singleton().SpawnEnemyTank(mDeltaTime);
	GameLevel::Singleton().SpawnEnemyTank(mDeltaTime);
}

void BattleCity::SetGameState(GameState gameState)
{
	mGameState = gameState;
}
