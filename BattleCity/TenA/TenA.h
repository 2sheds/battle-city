#pragma once
#include "../Framework.h"
#include "../BattleCity.h"



//enum class GameState {
//	PREPARING = 0,
//	IN_GAME,
//	GAME_OVER_LOST,
//	GAME_OVER_WIN,
//	COUNT
//};

class TenA :
	public Framework
{
private:
	GameState mGameState;
	int APressedCounter;

	//Sprite* mSpriteStart;
	//Sprite* mSpriteInGame;
	//Sprite* mSpriteLost;
	//Sprite* mSpriteWin;
	
	Sprite* mCurrentSprite;


	long long mLastTick;
	long long mCurrentTick;
	long long mDeltaTime;
	long long mAppTime;


public:
	virtual void PreInit(int& width, int& height, bool& fullscreen) override;

	virtual bool Init() override;

	virtual void Close() override;

	virtual bool Tick() override;

	void UpdateDeltaTime();

	virtual void onMouseMove(int x, int y, int xrelative, int yrelative) override;

	virtual void onMouseButtonClick(FRMouseButton button, bool isReleased) override;

	virtual void onKeyPressed(FRKey k) override;

	virtual void onKeyReleased(FRKey k) override;

	virtual const char* GetTitle() override;

	void Draw();
	
	void ResetGame();

	void GameStateChecker();

};

