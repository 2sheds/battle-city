#include "BattleCity.h"

int main(int argc, char *argv[])
{
	BattleCity* ptrBattleCity = new BattleCity();

	int w, h;

	if (UtilsLibrary::ParseMainArgsIntoResolution(argc, argv, w, h))
	{
		ptrBattleCity->mDefaultScreenWidth = w;
		ptrBattleCity->mDefaultScreenHeight = h;
	}

	return run(ptrBattleCity);
}
