#pragma once

#include "TankBase.h"
#include "Vec2D.h"

enum Effects {
	TANK_SPAWN,
	TANK_DESTROY,
	BULLET_IMPACT,
	BONUS_SPAWN,
	NUM_EFFECTS
};

enum LvlSpriteCollection{
	BACKGROUND_SPRITE,
	BONUS_SPRITE,
	RED_BRICK_SPRITE,
	STEEL_BRICK_SPRITE,
	PHOENIX_SPRITE,
	NUM_OBJECT_SPRITES
};

enum UIsprites {
	MAIN_MENU,
	GAME_OVER_WIN_MENU,
	GAME_OVER_LOST_MENU,
	TANK_ENEMY_ICON,
	PLAYER_LIVES_ICON,
	NUM_UI_SPRITES
};

class SpriteObject;

class SpriteLibrary

{
public:
	static void Init();
	static void SpawnFX(Effects effects, Vec2D pos, int animationSpeed = 100);

	static void Update(uint32_t deltaTime);
	static void Draw();
	static void DeSpawnEffect(SpriteObject* effect);

	static std::vector<Sprite*> GetTankSprites(Direction dir,
						TankType tankType = TankType::BASIC_TANK);

	static Sprite* GetLvlSpriteCollection(LvlSpriteCollection objectSprites);
	static Sprite* GetUISprites(UIsprites UIsprite);

private:

	SpriteLibrary();
	~SpriteLibrary();

	void SetupTankSprites();

	static SpriteLibrary* mInstance;

	std::vector<Sprite*> mEffectSprites [Effects::NUM_EFFECTS];
	std::vector<SpriteObject*> mEffects;

	Sprite* mLvlSpriteCollection[LvlSpriteCollection::NUM_OBJECT_SPRITES];
	Sprite* mUISprites[UIsprites::NUM_UI_SPRITES];

	std::vector<Sprite*> mTanksSprites[Effects::NUM_EFFECTS];
	std::vector<SpriteObject*> mTankTypeSprites;

	std::vector<Sprite*> mBasicTankSprites[Direction::NUM_DIRECTION];
	std::vector<Sprite*> mFastTankSprites[Direction::NUM_DIRECTION];
	std::vector<Sprite*> mPowerTankSprites[Direction::NUM_DIRECTION];
	std::vector<Sprite*> mArmorTankSprites[Direction::NUM_DIRECTION];
};

