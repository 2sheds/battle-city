#include "SpriteLibrary.h"
#include "FileManager.h"
#include <algorithm>

#include <string>

SpriteLibrary* SpriteLibrary::mInstance = nullptr;

void SpriteLibrary::SpawnFX(Effects effects, Vec2D pos, int animationSpeed)
{
	SpriteObject* NewFX = new SpriteObject();
	int spriteWidth, spriteHeight;
	getSpriteSize(mInstance->mEffectSprites[effects][0], spriteWidth, spriteHeight);
	NewFX->Init(mInstance->mEffectSprites[effects], pos.GetX() - spriteWidth / 2,
		pos.GetY() - spriteHeight / 2, animationSpeed, true, false);
	mInstance->mEffects.push_back(NewFX);
}

void SpriteLibrary::Update(uint32_t deltaTime)
{
	for (auto& effect : mInstance->mEffects)
	{
		effect->Update(deltaTime);
	}
}

void SpriteLibrary::Draw()
{
	for (auto& effect : mInstance->mEffects)
	{
		effect->Draw();
	}
}

void SpriteLibrary::DeSpawnEffect(SpriteObject* effect)
{
	mInstance->mEffects.erase(std::remove(mInstance->mEffects.begin(),
		mInstance->mEffects.end(), effect), mInstance->mEffects.end());

	delete effect;
}

SpriteLibrary::SpriteLibrary()
{
	std::string fullPath;

	/***
		FX section
	***/
	// bullet explosion
	mEffectSprites[Effects::BULLET_IMPACT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\explosion0.png", fullPath)));
	mEffectSprites[Effects::BULLET_IMPACT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\explosion1.png", fullPath)));
	mEffectSprites[Effects::BULLET_IMPACT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\explosion2.png", fullPath)));

	// tank destroy
	mEffectSprites[Effects::TANK_DESTROY].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-destroy0.png", fullPath)));
	mEffectSprites[Effects::TANK_DESTROY].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-destroy1.png", fullPath)));
	mEffectSprites[Effects::TANK_DESTROY].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-destroy2.png", fullPath)));
	mEffectSprites[Effects::TANK_DESTROY].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-destroy3.png", fullPath)));
	mEffectSprites[Effects::TANK_DESTROY].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-destroy4.png", fullPath)));

	// tank spawn
	mEffectSprites[Effects::TANK_SPAWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-spawn0.png", fullPath)));
	mEffectSprites[Effects::TANK_SPAWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\tank-spawn1.png", fullPath)));

	// bonus spawn
	mEffectSprites[Effects::BONUS_SPAWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\bonus-spawn0.png", fullPath)));
	mEffectSprites[Effects::BONUS_SPAWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\bonus-spawn1.png", fullPath)));
	mEffectSprites[Effects::BONUS_SPAWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\bonus-spawn2.png", fullPath)));
	mEffectSprites[Effects::BONUS_SPAWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\FX\\bonus-spawn3.png", fullPath)));


	/***
	Game Objects section
	***/

	SetupTankSprites();

	// game level objects
	mLvlSpriteCollection[LvlSpriteCollection::BACKGROUND_SPRITE] = createSprite
	(FileManager::GetFullPathToFile("\\data\\background.png", fullPath));
	mLvlSpriteCollection[LvlSpriteCollection::RED_BRICK_SPRITE] = createSprite
	(FileManager::GetFullPathToFile("\\data\\bricks\\red\\brick-whole.png", fullPath));
	mLvlSpriteCollection[LvlSpriteCollection::STEEL_BRICK_SPRITE] = createSprite
	(FileManager::GetFullPathToFile("\\data\\bricks\\steel\\brick-whole.png", fullPath));
	mLvlSpriteCollection[LvlSpriteCollection::PHOENIX_SPRITE] = createSprite
	(FileManager::GetFullPathToFile("\\data\\phoenix.png", fullPath));
	mLvlSpriteCollection[LvlSpriteCollection::BONUS_SPRITE] = createSprite
	(FileManager::GetFullPathToFile("\\data\\bonus.png", fullPath));

	// UI elements
	mUISprites[UIsprites::MAIN_MENU] = createSprite
	(FileManager::GetFullPathToFile("\\data\\UI\\mainMenu.png", fullPath));
	mUISprites[UIsprites::GAME_OVER_LOST_MENU] = createSprite
	(FileManager::GetFullPathToFile("\\data\\UI\\gameOverLost.png", fullPath));
	mUISprites[UIsprites::GAME_OVER_WIN_MENU] = createSprite
	(FileManager::GetFullPathToFile("\\data\\UI\\gameOverWin.png", fullPath));
	mUISprites[UIsprites::TANK_ENEMY_ICON] = createSprite
	(FileManager::GetFullPathToFile("\\data\\UI\\tank-enemy-icon.png", fullPath));
	mUISprites[UIsprites::PLAYER_LIVES_ICON] = createSprite
	(FileManager::GetFullPathToFile("\\data\\UI\\player-lives-icon.png", fullPath));

}


void SpriteLibrary::SetupTankSprites()
{
	std::string fullPath;

	// sprites for basic tank
	mBasicTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-up0.png", fullPath)));
	mBasicTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-up1.png", fullPath)));

	mBasicTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-down0.png", fullPath)));
	mBasicTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-down1.png", fullPath)));

	mBasicTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-left0.png", fullPath)));
	mBasicTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-left1.png", fullPath)));

	mBasicTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-right0.png", fullPath)));
	mBasicTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\basic-yellow-tank-right1.png", fullPath)));


	// sprites for fast tank
	mFastTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-up0.png", fullPath)));
	mFastTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-up1.png", fullPath)));

	mFastTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-down0.png", fullPath)));
	mFastTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-down1.png", fullPath)));

	mFastTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-left0.png", fullPath)));
	mFastTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-left1.png", fullPath)));

	mFastTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-right0.png", fullPath)));
	mFastTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\fast-grey-tank-right1.png", fullPath)));


	// sprites for power tank
	mPowerTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-up0.png", fullPath)));
	mPowerTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-up1.png", fullPath)));

	mPowerTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-down0.png", fullPath)));
	mPowerTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-down1.png", fullPath)));

	mPowerTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-left0.png", fullPath)));
	mPowerTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-left1.png", fullPath)));

	mPowerTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-right0.png", fullPath)));
	mPowerTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\power-green-tank-right1.png", fullPath)));


	// sprites for power tank
	mArmorTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-up0.png", fullPath)));
	mArmorTankSprites[Direction::TOP].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-up1.png", fullPath)));

	mArmorTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-down0.png", fullPath)));
	mArmorTankSprites[Direction::DOWN].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-down1.png", fullPath)));

	mArmorTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-left0.png", fullPath)));
	mArmorTankSprites[Direction::LEFT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-left1.png", fullPath)));

	mArmorTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-right0.png", fullPath)));
	mArmorTankSprites[Direction::RIGHT].push_back(createSprite
	(FileManager::GetFullPathToFile("\\data\\tanks\\armor-purple-tank-right1.png", fullPath)));

}

std::vector<Sprite*> SpriteLibrary::GetTankSprites(Direction dir, TankType tankType) {
	switch (tankType)
	{
	case BASIC_TANK:
		return mInstance->mBasicTankSprites[dir];
		break;
	case FAST_TANK:
		return mInstance->mFastTankSprites[dir];
		break;
	case POWER_TANK:
		return mInstance->mPowerTankSprites[dir];
		break;
	case ARMOR_TANK:
		return mInstance->mArmorTankSprites[dir];
		break;
	default:
		break;
	}
}

Sprite* SpriteLibrary::GetLvlSpriteCollection(LvlSpriteCollection objectSprites)
{
	return mInstance->mLvlSpriteCollection[objectSprites];
}

SpriteLibrary::~SpriteLibrary()
{
	delete mInstance;
}

void SpriteLibrary::Init()
{
	if (!mInstance)
	{
		mInstance = new SpriteLibrary();
	}
}

Sprite* SpriteLibrary::GetUISprites(UIsprites UIsprite)
{
	return mInstance->mUISprites[UIsprite];
}