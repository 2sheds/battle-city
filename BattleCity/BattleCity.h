#pragma once
#include "Brick.h"
#include "Bullet.h"
#include "EnemyTank.h"
#include "Framework.h"
#include "GameLevel.h"
#include "LevelBoundary.h"
#include "TankBase.h"
#include "PlayerTank.h"
#include <iostream>

enum class GameState {
	MAIN_MENU = 0,
	IN_GAME,
	GAME_OVER_LOST,
	GAME_OVER_WIN,
	NUM_GAME_STATE
};

class BattleCity :
	public Framework
{
public:
	~BattleCity();

	virtual void PreInit(int& width, int& height, bool& fullscreen) override;

	virtual bool Init() override;

	virtual void Close() override;

	virtual bool Tick() override;

	virtual void onMouseMove(int x, int y, int xrelative, int yrelative) override;

	virtual void onMouseButtonClick(FRMouseButton button, bool isReleased) override;

	virtual void onKeyPressed(FRKey k) override;
	void OnKeyPressedInGameState(FRKey k);

	virtual void onKeyReleased(FRKey k) override;

	virtual const char* GetTitle() override;

	void Draw();

	void ResetGame();

	void UpdateDeltaTime();

	static void SetGameState(GameState gameState);

	int mDefaultScreenWidth = MIN_RESOLUTION_WIDTH;
	int mDefaultScreenHeight = MIN_RESOLUTION_HEIGHT;

private:
	Sprite* mSpriteStart;
	Sprite* mSpriteLost;
	Sprite* mSpriteWin;
	Sprite* mCurrentSprite;

	static GameState mGameState;

	uint32_t mLastTick;
	uint32_t mCurrentTick;
	uint32_t mDeltaTime;
	
	bool mIsExited;

	LevelBoundary mLevelBoundary;
	Bullet mBullet;
};

