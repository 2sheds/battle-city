#include "Phoenix.h"
#include "../BattleCity.h"
#include "SpriteLibrary.h"
#include "FileManager.h"

#include <string>

void Phoenix::Init(int gameLvlWidth, int gameLvlHight)
{
	std::string fullPath;

	mSpriteObject.Init(SpriteLibrary::GetLvlSpriteCollection
					   (LvlSpriteCollection::PHOENIX_SPRITE), 
					   (gameLvlWidth - 40) / 2, gameLvlHight - 40);
	mHp = PHOENIX_HP;
	Actor::Init();
}

bool Phoenix::IsDestroy()
{
	return mHp <= 0;
}

void Phoenix::Destroy()
{
	SpriteLibrary::SpawnFX(Effects::TANK_DESTROY, mSpriteObject.GetCenterPoint());
	BattleCity::SetGameState(GameState::GAME_OVER_LOST);
}

void Phoenix::ReceiveDmg()
{
	mHp--;
	if (mHp <= 0)
	{
		Destroy();
	}
}


void Phoenix::Update()
{
}

void Phoenix::CollisionWithBullet(Bullet& bullet)
{
		if (mSpriteObject.Intersects(bullet.mSpriteObject))
		{
			ReceiveDmg();
			bullet.DestroyBullet();
		}
}
