#pragma once

#include "AARectangle.h"
#include "BoundaryEdges.h"
#include "SpriteObject.h"

class Bullet;
class TankBase;

class LevelBoundary
{
public:
	LevelBoundary() {}
	void Init(const int width, const int height);
	inline const AARectangle& GetAARectangle() const { return mBoundaryRect; }
	bool CollidedWithBoundary(SpriteObject& spriteObject);
	void CollidedBulletWithBoundary(TankBase& shooter);

private:
	void SetupEdges();
	AARectangle mBoundaryRect;

	BoundaryEdge mEdges[NUM_EDGES];
};

