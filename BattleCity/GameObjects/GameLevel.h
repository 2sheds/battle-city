#pragma once

#include "Utils.h"
#include "Bonus.h"
#include "PlayerTank.h"
#include "UserInterface.h"
#include "Brick.h"
#include "EnemyTank.h"
#include "LevelBoundary.h"
#include "Phoenix.h"

#include <vector>

class AARectangle;
class ButtleCity;
class Bullet;
class TankBase;
class PlayerTank;

class GameLevel
{
public:
	static GameLevel& Singleton();
	void Draw();
	void Init();
	void Update(Bullet& bullet, uint32_t mDeltaTime);
	void SpawnEnemyTank(uint32_t deltaTime);
	bool IsCellFree(Vec2D pos);

	static const int NUM_LEVEL_ROWS = 13;
	static const int NUM_LEVEL_COLS = 13;
	const int LEVEL_WIDTH = NUM_LEVEL_COLS * CELL_SIDE_SIZE;
	const int LEVEL_HEIGHT = NUM_LEVEL_ROWS * CELL_SIDE_SIZE;
	const int ENEMY_TANKS_LIMIT = 20;
	int mEnemyTanksToKill;
	bool wasSpawn = true;
	PlayerTank mPlayerTank;


private:
	void IsGameOver(uint32_t deltaTime);
	void SetupLevelGrid();
	int FindSpawnPoint();
	void CreateDefaultLevel();
	void CreateBrick(int xIndex, int yIndex,
		BrickType brickType, BrickMaterial brickMaterial);
	

	Brick mLevelGrid[NUM_LEVEL_ROWS][NUM_LEVEL_COLS];
	Bonus mBonus;
	SpriteObject mBackground;
	Phoenix mPhoenix;
	LevelBoundary mLevelBoundary;
	UserInterface mUserInterface;
	std::vector<AARectangle> mAvailableSpawnSpots;
	std::vector<EnemyTank> mEnemyTanks;
	EnemyTank mEnemyTank;
	bool mIsGameOver = false;
	bool needSpawnTank;

	const int MAX_ENEMY_TANKS = 2 - 1;
};

