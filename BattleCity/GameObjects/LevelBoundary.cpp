#include "LevelBoundary.h"
#include "Bullet.h"

#include "TankBase.h"
#include "Utils.h"

void LevelBoundary::Init(const int width, const int height)
{
	mBoundaryRect.Init(0, 0, width, height);
	SetupEdges();
}

bool LevelBoundary::CollidedWithBoundary(SpriteObject& spriteObject)
{
	if (mBoundaryRect.GetTopLeftPoint().GetX() > spriteObject.GetTopLeftPoint().GetX())
	{
		spriteObject.MoveTo(Vec2D(mBoundaryRect.GetTopLeftPoint().GetX(),
			                      spriteObject.GetTopLeftPoint().GetY()));
		return true;
	}
	else if (spriteObject.GetBottomRightPoint().GetX() > mBoundaryRect.GetBottomRightPoint().GetX())
	{
		spriteObject.MoveTo(Vec2D(mBoundaryRect.GetBottomRightPoint().GetX() -
			spriteObject.GetSOWidth(), spriteObject.GetTopLeftPoint().GetY()));
		return true;
	}
	else if (mBoundaryRect.GetTopLeftPoint().GetY() > spriteObject.GetTopLeftPoint().GetY())
	{
		spriteObject.MoveTo(Vec2D(spriteObject.GetTopLeftPoint().GetX(),
								  mBoundaryRect.GetTopLeftPoint().GetY()));
		return true;
	}
	else if (mBoundaryRect.GetBottomRightPoint().GetY() <
			 spriteObject.GetBottomRightPoint().GetY())
	{
		spriteObject.MoveTo(Vec2D(spriteObject.GetTopLeftPoint().GetX(),
		  mBoundaryRect.GetBottomRightPoint().GetY() -
		  spriteObject.GetHeight()));
		return true;
	}
	return false;
}

void LevelBoundary::CollidedBulletWithBoundary(TankBase& shooter)
{
	Bullet& bullet = shooter.mBullet;
	if ((mBoundaryRect.GetTopLeftPoint().GetX() > bullet.mSpriteObject.GetTopLeftPoint().GetX()) ||
		(bullet.mSpriteObject.GetBottomRightPoint().GetX() > mBoundaryRect.GetBottomRightPoint().GetX()) ||
		(mBoundaryRect.GetTopLeftPoint().GetY() > bullet.mSpriteObject.GetTopLeftPoint().GetY()) ||
		(mBoundaryRect.GetBottomRightPoint().GetY() < bullet.mSpriteObject.GetBottomRightPoint().GetY()))
	{
		shooter.mBullet.mWasLaunched = false;
		shooter.mBullet.DestroyBullet();
	}

}


void LevelBoundary::SetupEdges()
{
	mEdges[TOP_EDGE].edge = { mBoundaryRect.GetTopLeftPoint().GetX(), mBoundaryRect.GetTopLeftPoint().GetY(), 
		mBoundaryRect.GetBottomRightPoint().GetX(), mBoundaryRect.GetTopLeftPoint().GetY() };
	mEdges[TOP_EDGE].normal = DOWN_DIR;

	mEdges[LEFT_EDGE].edge = { mBoundaryRect.GetTopLeftPoint().GetX(), mBoundaryRect.GetTopLeftPoint().GetY(), 
		mBoundaryRect.GetTopLeftPoint().GetX(), mBoundaryRect.GetBottomRightPoint().GetY() };
	mEdges[LEFT_EDGE].normal = RIGHT_DIR;

	mEdges[BOTTOM_EDGE].edge = { mBoundaryRect.GetTopLeftPoint().GetX(), mBoundaryRect.GetBottomRightPoint().GetY(), 
		mBoundaryRect.GetBottomRightPoint().GetX(), mBoundaryRect.GetBottomRightPoint().GetY() };
	mEdges[BOTTOM_EDGE].normal = UP_DIR;

	mEdges[RIGHT_EDGE].edge = { mBoundaryRect.GetBottomRightPoint().GetX(), mBoundaryRect.GetTopLeftPoint().GetY(), 
	mBoundaryRect.GetBottomRightPoint().GetX(), mBoundaryRect.GetBottomRightPoint().GetY() };
	mEdges[RIGHT_EDGE].normal = LEFT_DIR;
}

