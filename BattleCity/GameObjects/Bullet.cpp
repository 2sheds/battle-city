#include "Bullet.h"
#include "SpriteLibrary.h"
#include "TankBase.h"
#include "FileManager.h"
#include "Utils.h"

#include <string>



void Bullet::Init(TankBase& shooter)
{
	std::string filePath;
	mSpriteObject.mSprite = createSprite
				(FileManager::GetFullPathToFile("\\data\\bullet.png", filePath));
	getSpriteSize(mSpriteObject.mSprite, mWidth, mHeight);
	SetupBulletDirection(shooter);

	switch (shooter.GetTankType())
	{
	case BASIC_TANK:
		mVelocity = SLOW_BULLET;
		mDmg = DEFAULT_DMG;
		mBulletType = TIER_ONE_BULLET;
		break;
	case FAST_TANK:
		mVelocity = NORMAL_BULLET;
		mDmg = DEFAULT_DMG;
		mBulletType = TIER_TWO_BULLET;
		break;
	case POWER_TANK:
		mVelocity = FAST_BULLET;
		mDmg = DEFAULT_DMG;
		mBulletType = TIER_THREE_BULLET;
		break;
	case ARMOR_TANK:
		mVelocity = NORMAL_BULLET;
		mDmg = INCREASED_DMG;
		mBulletType = TIER_FOUR_BULLET;
		break;
	default:
		break;
	}
	mSpriteObject.Init(mSpriteObject.mSprite, mXpos, mYpos);

	mWasLaunched = true;

	Actor::Init();
	mSpriteObject.MoveTo(Vec2D(mXpos, mYpos));
}

void SetupBulletType(TankBase& shooter)
{

}

void Bullet::SetupBulletDirection(TankBase& shooter)
{
	mDirection = shooter.GetGunDirection();

	if (mDirection == Vec2D::Zero || mDirection == UP_DIR)
	{
		mDirection = UP_DIR;
		mXpos = shooter.mSpriteObject.GetSOXpos() + shooter.mSpriteObject.GetSOWidth()
			/ 2 - mWidth / 2;
		mYpos = shooter.mSpriteObject.GetSOYpos() - mHeight / 2;
	}
	else if (mDirection == DOWN_DIR)
	{
		mXpos = shooter.mSpriteObject.GetSOXpos() + shooter.mSpriteObject.GetSOWidth()
			/ 2 - mWidth / 2;
		mYpos = shooter.mSpriteObject.GetSOYpos() + shooter.mSpriteObject.GetSOHeight() / 2 - mHeight / 2;
	}
	else if (mDirection == LEFT_DIR)
	{
		mXpos = shooter.mSpriteObject.GetSOXpos() - mWidth / 2;
		mYpos = shooter.mSpriteObject.GetSOYpos() + shooter.mSpriteObject.GetSOHeight() / 2 - mHeight / 2;
	}
	else if (mDirection == RIGHT_DIR)
	{
		mXpos = shooter.mSpriteObject.GetSOXpos() + shooter.mSpriteObject.GetSOWidth() - mWidth / 2;
		mYpos = shooter.mSpriteObject.GetSOYpos() + shooter.mSpriteObject.GetSOHeight() / 2 - mHeight / 2;
	}
}

void Bullet::Update(uint32_t deltaTime)
{
	if (mWasLaunched)
	{
		mSpriteObject.MoveBy(mDirection * mVelocity * UtilsLibrary::MillisecondsToSeconds(deltaTime));
		Vec2D a = mDirection * mVelocity * UtilsLibrary::MillisecondsToSeconds(deltaTime);
	}
}

void Bullet::Draw()
{
	if (mWasLaunched)
	{
		mSpriteObject.Draw();
	}
}

void Bullet::DestroyBullet()
{
	mVelocity = 0;
	mWasLaunched = false;
	SpriteLibrary::SpawnFX(Effects::BULLET_IMPACT, mSpriteObject.GetCenterPoint());

}
