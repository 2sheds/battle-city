#pragma once
#include "TankBase.h"

class PlayerTank :
	public TankBase
{
public:
	virtual void Init(Vec2D defaultSpawnPoint = DEFAUL_SPAWN_POINT,
		TankType tankType = TankType::BASIC_TANK) override;

	virtual void SetupDirections() override;
	virtual void InitSpriteObject(Vec2D defaultSpawnPoint = DEFAUL_SPAWN_POINT) override;
	virtual void Update(uint32_t deltaTime) override;
	virtual void DestroyTank() override;
	void ReceiveBonus();
	int mCurrentLives = DEFAULT_LIVES_NUM;
	const int DEFAULT_LIVES_NUM = 3;

private:
	virtual void ReceiveDmg(Bullet& bullet);
};
