#pragma once

#include "Actor.h"
#include "Bullet.h"
#include "Utils.h"
#include <vector>

enum TankType{
	BASIC_TANK = 0, // default
	FAST_TANK,		// second tier
	POWER_TANK,		// third tier
	ARMOR_TANK,		// fourth tier
	NUM_TANK_TYPE
};

enum TankSpeed {
	SLOW = 60,		// BASIC_TANK - default
	NORMAL = 90,	// ARMOR_TANK, POWER_TANK - third tier, fourth tier
	FAST = 110,		// FAST_TANK - second tier
	NUM_TANK_SPEED
};

class TankBase :
	public Actor
{
public:
	virtual void Init(Vec2D defaultSpawnPoint = DEFAUL_SPAWN_POINT,
			  TankType tankType = TankType::BASIC_TANK);
	virtual void Update(uint32_t deltaTime) = 0;
	void Draw();

	void SetMovementDirection(Direction dir);
	void UnsetMovementDirection(Vec2D dir);
	inline void StopMovement() { mDirection = Vec2D::Zero; }
	inline Vec2D GetDirection() { return mDirection; }
	inline Vec2D GetGunDirection() { return mGunDirection; }
	inline TankType GetTankType() { return mTankType; }
	virtual void Shot();
	virtual void ReceiveDmg(Bullet& bullet) = 0;
	void CollisionWithBullet(Bullet& bullet);

	bool mIsAlife;
	static const Vec2D DEFAUL_SPAWN_POINT;

	Bullet mBullet;

protected:

	virtual void SetupDirections() = 0;
	virtual void InitSpriteObject(Vec2D defaultSpawnPoint) = 0;
	virtual void DestroyTank() = 0;

	TankSpeed mVelocity;
	Vec2D mDirection;
	Vec2D mGunDirection;
	int mHp;
	const int DEFAULT_HP = 1;
	const int INCREASED_HP = 4;
	

	std::vector<Sprite*> SpriteDirections[Direction::NUM_DIRECTION];

	TankType mTankType;

private:
	void SetupSprites(Vec2D& defaultSpawnPoint);
};

