#include "SpriteObject.h"
#include "SpriteLibrary.h"
#include "Vec2D.h"

#include <iostream>

SpriteObject::~SpriteObject()
{}

void SpriteObject::Draw()
{
	if (!mIsAnimLoopEnded)
	{
		drawSprite(mSprite, mXpos, mYpos);
	}
}

void SpriteObject::Update(int deltaTime)
{
	if (mIsAnimated && mSprites.size())
	{
		mCurrentFrameTime += deltaTime;

		if (mCurrentFrameTime > mAnimSpeed)
		{
			if (mCurrentFrame == mMaxFrames)
			{
				if (mIsLooped) {
					mCurrentFrame = 0;
				}
				else
				{
					mIsAnimLoopEnded = true;
					SpriteLibrary::DeSpawnEffect(this);
					return;
 				}
			}
			mSprite = mSprites[mCurrentFrame];
			mCurrentFrame += 1;
			
			mCurrentFrameTime = 0;
		}
	}

}

void SpriteObject::Init(Sprite* sprite, int xPos, int yPos)
{
	mSprite = sprite;
	mXpos = xPos;
	mYpos = yPos;
	getSpriteSize(sprite, mWidth, mHeight);
	mIsAnimated = false;

	ClearPoints();
	AARectangle::Init(mXpos, mYpos, mWidth, mHeight);
}

void SpriteObject::Init(std::vector<Sprite*> sprites, int xPos, int yPos,
						int animSpeed, bool startsAnimated, bool isLooped)
{
	mSprites = sprites;
	mMaxFrames = mSprites.size();
	mSprite = mSprites.front();

	mXpos = xPos;
	mYpos = yPos;
	getSpriteSize(mSprite, mWidth, mHeight);
	mIsAnimated = startsAnimated;
	mAnimSpeed = animSpeed;
	
	ClearPoints();
	AARectangle::Init(mXpos, mYpos, mWidth, mHeight);

	mIsLooped = isLooped;
}

void SpriteObject::SetupPosition(Vec2D topLeftPoint) {
	mXpos = topLeftPoint.GetX();
	mYpos = topLeftPoint.GetY();
	AARectangle::Init(mXpos, mYpos, mWidth, mHeight);
}

void SpriteObject::MoveBy(const Vec2D& deltaOffset)
{
	AARectangle::MoveBy(deltaOffset);
	SetupPosition(GetTopLeftPoint());
}

void SpriteObject::MoveTo(const Vec2D& deltaOffset)
{
	AARectangle::MoveTo(deltaOffset);
	SetupPosition(GetTopLeftPoint());
}

void SpriteObject::SetAnimatedSprites(std::vector<Sprite*> sprites) 
{
	mSprites = sprites;
	mSprite = mSprites.front();
}

void SpriteObject::SetAnimationEnabled(bool isEnabled)
{
	mIsAnimated = isEnabled;
}
