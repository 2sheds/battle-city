#pragma once
#include "Actor.h"

enum BrickMaterial
{
	RED_BRICK,
	STEEL_BRICK,
	NUM_BRICK_MATERIAL
};

enum BrickType
{
	WHOLE_BRICK = 0,
	TOP_BREAK,
	DOWN_BREAK,
	LEFT_BREAK,
	RIGHT_BREAK,
	NUM_BRICK_TYPE
};


class Brick :
	public Actor
{
public:
	void Init(const BrickType brickType, const BrickMaterial brickMatherial,
			  const int xPos, const int yPos);
	void Draw();
	void Destroy(int dmg);

	inline bool IsDestroyed() const { return !mHp; }
	inline int GetWidth() const { return mWidth; }
	inline int GetHeight() const { return mHeight; }
	inline int GetHP() const { return mHp; }
	inline int GetXpos() const { return mXpos; }
	inline int GetYpos() const { return mYpos; }

	inline void SetXpos(int xPos) { mXpos = xPos; }
	inline void SetYpos(int yPos) { mYpos = yPos; }

private:
	int mHp = 0;
	int mXpos, mYpos;
	int mWidth, mHeight;
	BrickType mBrickType;
	BrickMaterial mBrickMaterial;

	const int RED_BRICK_HP = 4;
	const int STEEL_BRICK_HP = 10;
};

