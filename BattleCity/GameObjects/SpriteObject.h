#pragma once

#include "AARectangle.h"
#include "../Framework.h"


class SpriteObject :
	public AARectangle
{
public:
	~SpriteObject();
	void Init(Sprite* sprite, int xPos, int yPos);
	void Init(std::vector<Sprite*> sprites, int xPos, int yPos,
			  int animSpeed = 100, bool startsAnimated = false, bool isLooped = true);
	virtual void Draw();
	void Update(int deltaTime);
	
	void SetupPosition(Vec2D topLeftPoint);
	void MoveBy(const Vec2D& deltaOffset) override;
	void MoveTo(const Vec2D& position) override;

	inline int GetSOWidth() const { return mWidth; }
	inline int GetSOHeight() const { return mHeight; }
	inline int GetSOXpos() const { return mXpos; }
	inline int GetSOYpos() const { return mYpos; }

	void SetAnimatedSprites(std::vector<Sprite*> sprites);
	void SetAnimationEnabled(bool isEnabled);

	Sprite* mSprite;
	int mXpos, mYpos;
private:
	bool mIsAnimated;
	int mWidth, mHeight;

	int mCurrentFrameTime;
	int mAnimSpeed;
	int mCurrentFrame = 0;
	int mMaxFrames;
	std::vector<Sprite*> mSprites;
	bool mIsLooped;
	bool mIsAnimLoopEnded;
};
