#include "TankBase.h"
#include "SpriteLibrary.h"
#include "Bullet.h"
#include "EnemyTank.h"
#include "FileManager.h"

#include "PlayerTank.h"
#include "Utils.h"

const Vec2D TankBase::DEFAUL_SPAWN_POINT = { 6 * CELL_SIDE_SIZE, 6 * CELL_SIDE_SIZE };

void TankBase::Init(Vec2D defaultSpawnPoint, TankType tankType)
{
	mTankType = tankType;
	SetupSprites(defaultSpawnPoint);

	switch (tankType)
	{
	case BASIC_TANK:
		mHp = DEFAULT_HP;
		mVelocity = TankSpeed::SLOW;
		break;
	case FAST_TANK:
		mHp = DEFAULT_HP;
		mVelocity = TankSpeed::FAST;
		break;
	case POWER_TANK:
		mHp = DEFAULT_HP;
		mVelocity = TankSpeed::NORMAL;
		break;
	case ARMOR_TANK:
		mHp = INCREASED_HP;
		mVelocity = TankSpeed::NORMAL;
		break;
	default:
		break;
	}

	SetupDirections();
	mIsAlife = true;
	Actor::Init();
}

void TankBase::SetupSprites(Vec2D& defaultSpawnPoint)
{
	InitSpriteObject(defaultSpawnPoint);
}

void TankBase::Draw()
{

	mSpriteObject.Draw();
	mBullet.Draw();
}

void TankBase::SetMovementDirection(Direction dir)
{
	switch (dir)
	{
	case TOP:
		mDirection = UP_DIR;
		mGunDirection = UP_DIR;
		break;
	case DOWN:
		mDirection = DOWN_DIR;
		mGunDirection = DOWN_DIR;
		break;
	case LEFT:
		mDirection = LEFT_DIR;
		mGunDirection = LEFT_DIR;
		break;
	case RIGHT:
		mDirection = RIGHT_DIR;
		mGunDirection = RIGHT_DIR;
		break;
	default:
		break;
	}
	mSpriteObject.SetAnimatedSprites(SpriteLibrary::GetTankSprites(dir, mTankType));
	mSpriteObject.SetAnimationEnabled(true);
}

void TankBase::UnsetMovementDirection(Vec2D dir)
{
	if (mDirection == dir)
	{
		mDirection = Vec2D::Zero;
		mSpriteObject.SetAnimationEnabled(false);
	}
}

void TankBase::Shot()
{
	mBullet.Init(*this);
}

void TankBase::CollisionWithBullet(Bullet& bullet)
{
	if (mSpriteObject.Intersects(bullet.mSpriteObject))
	{
		ReceiveDmg(bullet);
		bullet.DestroyBullet();
	}
}
