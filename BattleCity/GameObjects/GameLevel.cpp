
#include "GameLevel.h"
#include "Bullet.h"
#include "SpriteLibrary.h"

#include "FileManager.h"
#include "PlayerTank.h"
#include "../Framework.h"
#include "../BattleCity.h"

GameLevel& GameLevel::Singleton()
{
	static GameLevel theGameLevel;
	return theGameLevel;
}

void GameLevel::Init()
{
	mBonus.Init();
	mEnemyTanksToKill = ENEMY_TANKS_LIMIT;
	mLevelBoundary.Init(BATTLEFIELD_WIDTH, BATTLEFIELD_HEIGHT);
	std::string fullPath;

	mBackground.Init(SpriteLibrary::GetLvlSpriteCollection
					 (LvlSpriteCollection::BACKGROUND_SPRITE), 0, 0);
	mEnemyTanks.clear();
	CreateDefaultLevel();
	mUserInterface.Init();
}

void GameLevel::Draw()
{
	drawSprite(mBackground.mSprite, 0, 0);

	for (int i = 0; i < NUM_LEVEL_COLS; i++)
	{
		for (int j = 0; j < NUM_LEVEL_ROWS; j++)
		{
			if (!(mLevelGrid[i][j].IsDestroyed()))
			{
				mLevelGrid[i][j].Draw();
			}
		}
	}

	drawSprite(mPhoenix.mSpriteObject.mSprite, mPhoenix.mSpriteObject.GetSOXpos(),
		mPhoenix.mSpriteObject.GetSOYpos());

	for (auto &e : mEnemyTanks)
	{
		if (e.mIsAlife)
		{
			e.Draw();
		}
	}
	mBonus.Draw();
}

void GameLevel::CreateBrick(int xIndex, int yIndex,
							BrickType brickType, BrickMaterial brickMaterial)
{
	mLevelGrid[yIndex][xIndex].Init(brickType, brickMaterial,
								   xIndex * 40, yIndex * 40);
}

void GameLevel::CreateDefaultLevel()
{
	SetupLevelGrid();

	CreateBrick( 2,1, WHOLE_BRICK, RED_BRICK);
	CreateBrick( 3,1, WHOLE_BRICK, RED_BRICK);
	CreateBrick( 6,1, WHOLE_BRICK, STEEL_BRICK);
	CreateBrick(9, 1, WHOLE_BRICK, RED_BRICK);
	CreateBrick(10, 1, WHOLE_BRICK, RED_BRICK);

	CreateBrick(2, 2, WHOLE_BRICK, RED_BRICK);
	CreateBrick(3, 2, WHOLE_BRICK, RED_BRICK);
	CreateBrick(6, 2, WHOLE_BRICK, STEEL_BRICK);
	CreateBrick(9, 2, WHOLE_BRICK, RED_BRICK);
	CreateBrick(10, 2, WHOLE_BRICK, RED_BRICK);

	CreateBrick(2, 3, WHOLE_BRICK, RED_BRICK);
	CreateBrick(3, 3, WHOLE_BRICK, RED_BRICK);
	CreateBrick(6, 3, WHOLE_BRICK, STEEL_BRICK);
	CreateBrick(9, 3, WHOLE_BRICK, RED_BRICK);
	CreateBrick(10, 3, WHOLE_BRICK, RED_BRICK);

	CreateBrick(2, 4, WHOLE_BRICK, RED_BRICK);
	CreateBrick(3, 4, WHOLE_BRICK, RED_BRICK);
	CreateBrick(6, 4, WHOLE_BRICK, STEEL_BRICK);
	CreateBrick(9, 4, WHOLE_BRICK, RED_BRICK);
	CreateBrick(10, 4, WHOLE_BRICK, RED_BRICK);

	CreateBrick(2, 3, WHOLE_BRICK, RED_BRICK);
	CreateBrick(10, 3, WHOLE_BRICK, RED_BRICK);
	CreateBrick(2, 4, WHOLE_BRICK, RED_BRICK);
	CreateBrick(10, 4, WHOLE_BRICK, RED_BRICK);

	for (int i = 0; i < 4; i++)
	{
		CreateBrick(i, 7, WHOLE_BRICK, RED_BRICK);
	}

	for (int i = 9; i < 13; i++)
	{
		CreateBrick(i, 7, WHOLE_BRICK, RED_BRICK);
	}

	CreateBrick(5, 9, WHOLE_BRICK, RED_BRICK);
	CreateBrick(6, 9, WHOLE_BRICK, RED_BRICK);
	CreateBrick(7, 9, WHOLE_BRICK, RED_BRICK);
	
	CreateBrick(5, 12, WHOLE_BRICK, RED_BRICK);
	CreateBrick(5, 11, WHOLE_BRICK, RED_BRICK);
	CreateBrick(6, 11, WHOLE_BRICK, RED_BRICK);
	CreateBrick(7, 11, WHOLE_BRICK, RED_BRICK);
	CreateBrick(7, 12, WHOLE_BRICK, RED_BRICK);
	

	for (int i = 1, j = 10; i < 4 && j < 12; i++, j++)
	{
		CreateBrick(i, 10, WHOLE_BRICK, STEEL_BRICK);
		CreateBrick(j, 10, WHOLE_BRICK, STEEL_BRICK);
	}

	mPhoenix.Init(LEVEL_WIDTH, LEVEL_HEIGHT);
}

void GameLevel::Update(Bullet& bullet, uint32_t deltaTime)
{
	
	//collision player/enemy tanks with lvl boundary
	mLevelBoundary.CollidedWithBoundary(mPlayerTank.mSpriteObject);
	for (auto &e : mEnemyTanks)
	{
		if (e.mIsAlife)
		{
			if (mLevelBoundary.CollidedWithBoundary(e.mSpriteObject))
			{
				e.FindMovementDirection();
			}
		}
	}

	//collision player/enemy tanks bullet with lvl boundary
	if (mPlayerTank.mBullet.mWasLaunched)
	{
		mLevelBoundary.CollidedBulletWithBoundary(mPlayerTank);
	}
	for (auto &e : mEnemyTanks)
	{
		if (e.mIsAlife)
		{
			if (e.mBullet.mWasLaunched)
			{
				mLevelBoundary.CollidedBulletWithBoundary(e);
			}
		}
	}


	BoundaryEdge edge;

	for (int i = 0; i < NUM_LEVEL_COLS; i++)
	{
		for (int j = 0; j < NUM_LEVEL_ROWS; j++)
		{
			if (!(mLevelGrid[i][j].IsDestroyed()))
			{
				//collision with player tank
				if (mLevelGrid[i][j].HasCollided(mPlayerTank.mSpriteObject, edge))
					{
						Vec2D pointOnEdge;
						mPlayerTank.MakeFlushWithEdge(edge, pointOnEdge, true);
						
					}
				if (mPlayerTank.mBullet.mWasLaunched)
				{
					if (mLevelGrid[i][j].HasCollided(mPlayerTank.mBullet.mSpriteObject, edge))
					{
						mLevelGrid[i][j].Destroy(mPlayerTank.mBullet.GetBulletDmg());
						mPlayerTank.mBullet.DestroyBullet();
					}
				}

				//collision with enemy tanks
				for (auto &e : mEnemyTanks)
				{
					if (e.mIsAlife)
					{
						if (mLevelGrid[i][j].HasCollided(e.mSpriteObject, edge))
						{
							Vec2D pointOnEdge;
							e.MakeFlushWithEdge(edge, pointOnEdge, true);
							e.FindMovementDirection();
						}
						if (e.mBullet.mWasLaunched)
						{
							if (mLevelGrid[i][j].HasCollided(e.mBullet.mSpriteObject, edge))
							{
								mLevelGrid[i][j].Destroy(e.mBullet.GetBulletDmg());
								e.mBullet.DestroyBullet();
							}
						}
					}
				}
			}
		}
	}


	//collision enemy tank and player tank
	for (auto &e : mEnemyTanks)
	{
		if (e.mIsAlife)
		{
			if (e.HasCollided(mPlayerTank.mSpriteObject, edge))
			{
				Vec2D pointOnEdge;
				mPlayerTank.MakeFlushWithEdge(edge, pointOnEdge, true);
			}
		}
	}


	//collision enemy/player tanks with bullets
	for (auto &e : mEnemyTanks)
	{
		if (e.mIsAlife)
		{
			if (e.mBullet.mWasLaunched)
			{
				mPlayerTank.CollisionWithBullet(e.mBullet);
				mPhoenix.CollisionWithBullet(e.mBullet);
			}

			if (mPlayerTank.mBullet.mWasLaunched)
			{
				e.CollisionWithBullet(mPlayerTank.mBullet);
			}
		}
	}


	//collision with phoenix
	if (mPhoenix.HasCollided(mPlayerTank.mSpriteObject, edge))
	{ 
		Vec2D pointOnEdge;
		mPlayerTank.MakeFlushWithEdge(edge, pointOnEdge, true);
	}
	if (mPlayerTank.mBullet.mWasLaunched)
	{
		mPhoenix.CollisionWithBullet(mPlayerTank.mBullet);
	}

	for (auto &i : mEnemyTanks)
	{
		if (i.mIsAlife)
		{
			if (mPhoenix.HasCollided(i.mSpriteObject, edge))
			{
				Vec2D pointOnEdge;
				i.MakeFlushWithEdge(edge, pointOnEdge, true);
			}
		}
	}


	//mEnemyTanks[0].Update(deltaTime);
	for (auto &i : mEnemyTanks)
	{
		if (i.mIsAlife)
		{
			i.Update(deltaTime);
		}
	}


	SpawnEnemyTank(deltaTime);

	IsGameOver(deltaTime);

	mUserInterface.Update();

	SpriteLibrary::Update(deltaTime);

	mBonus.Update(deltaTime);
}

bool GameLevel::IsCellFree(Vec2D pos)
{
	if (!mLevelBoundary.GetAARectangle().ContainsPoint(pos))
	{
		return false;
	}

	int cellRow = pos.GetX();

	int cellCol = pos.GetY();

	AARectangle freeSpot;
	freeSpot.Init(cellRow, cellCol, CELL_SIDE_SIZE, CELL_SIDE_SIZE);

	cellRow /= CELL_SIDE_SIZE;
	cellCol /=  CELL_SIDE_SIZE;
	
	if ( !mLevelGrid[cellCol][cellRow].IsDestroyed() ||
		  mPlayerTank.mSpriteObject.Intersects(freeSpot) )
	{
		return false;
	}

	return true;
}


int GameLevel::FindSpawnPoint()
{
	int randomRow = UtilsLibrary::GetRandomNumber(2);
	for (int i = 0; i < NUM_LEVEL_ROWS; i++)
	{
		if (mLevelGrid[randomRow][i].IsDestroyed())
		{
			AARectangle freeSpot;
			freeSpot.Init(mLevelGrid[randomRow][i].GetXpos(),
				mLevelGrid[randomRow][i].GetYpos(),
				CELL_SIDE_SIZE, CELL_SIDE_SIZE);

			if (!mPlayerTank.mSpriteObject.Intersects(freeSpot))
			{
				mAvailableSpawnSpots.push_back(freeSpot);
			}
		}
	}

	int spawnPointIndex = UtilsLibrary::GetRandomNumber(mAvailableSpawnSpots.size() - 1);

	return spawnPointIndex;
}

void GameLevel::IsGameOver(uint32_t deltaTime)
{
	if (mEnemyTanks.size() >= ENEMY_TANKS_LIMIT)
	{
		mIsGameOver = true;
		for (auto& e : mEnemyTanks)
		{
			if (e.mIsAlife)
			{
				mIsGameOver = false;
			}
		}
	}
	if (mIsGameOver)
	{
		BattleCity::SetGameState(GameState::GAME_OVER_WIN);
	}
	mIsGameOver = false;
}

void GameLevel::SpawnEnemyTank(uint32_t deltaTime)
{
	if (mEnemyTanks.size() < ENEMY_TANKS_LIMIT)
	{
		needSpawnTank = true;
		int numAliveEnemies = 0;
		for (auto& e : mEnemyTanks)
		{
			if (e.mIsAlife)
			{
				++numAliveEnemies;
			}
		}
		if (numAliveEnemies > MAX_ENEMY_TANKS)
		{
			needSpawnTank = false;
		}

		if (needSpawnTank)
		{
			int spawnPointIndex = FindSpawnPoint();
			int xPos = mAvailableSpawnSpots[spawnPointIndex].GetTopLeftPoint().GetX();
			int xYos = mAvailableSpawnSpots[spawnPointIndex].GetTopLeftPoint().GetY();

			TankType tankType = static_cast<TankType>(UtilsLibrary::GetRandomNumber(TankType::ARMOR_TANK));
			mEnemyTank.Init(Vec2D(xPos, xYos), tankType);
			mEnemyTank.MoveTank(deltaTime);
			
			mEnemyTanks.push_back(mEnemyTank);
			SpriteLibrary::SpawnFX(Effects::TANK_SPAWN, mEnemyTank.mSpriteObject.GetCenterPoint(), 300);

			int emenyTanksNum = mEnemyTanks.size();
			if (emenyTanksNum == 4 || emenyTanksNum == 11 || emenyTanksNum == 18)
			{
				mBonus.SpawnBonus();
			}
		}
	}
	
}


void GameLevel::SetupLevelGrid()
{
	for (int i = 0; i < NUM_LEVEL_COLS; i++ )
	{
		for (int j = 0; j < NUM_LEVEL_ROWS; j++ )
		{
			mLevelGrid[i][j].SetXpos(j * CELL_SIDE_SIZE);
			mLevelGrid[i][j].SetYpos(i * CELL_SIDE_SIZE);
		}
	}
}
