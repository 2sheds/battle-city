#include "Brick.h"
#include "SpriteLibrary.h"
#include "FileManager.h"
#include <iostream>

void Brick::Init(const BrickType brickType, const BrickMaterial brickMaterial,
				 const int xPos, const int yPos)
{
	mBrickType = brickType;
	mBrickMaterial = brickMaterial;
	mXpos = xPos;
	mYpos = yPos;

	std::string filePath;
	if (brickMaterial == RED_BRICK)
	{
		/*  in the process of developing 
		switch (brickType)
		{
		case WHOLE_BRICK:
			mSpriteObject.Init(createSprite
					  (FileManager::GetFullPathToFile("\\data\\bricks\\red\\brick-whole.png", filePath)), xPos, yPos);
			break;
		case TOP_BREAK:
			mSpriteObject.Init(createSprite
					  (FileManager::GetFullPathToFile("\\data\\bricks\\red\\brick-top.png", filePath)), xPos, yPos);
			break;
		case DOWN_BREAK:
			mSpriteObject.Init(createSprite
					  (FileManager::GetFullPathToFile("\\data\\bricks\\red\\brick-down.png", filePath)), xPos, yPos);
			break;
		case LEFT_BREAK:
			mSpriteObject.Init(createSprite
					  (FileManager::GetFullPathToFile("\\data\\bricks\\red\\brick-left.png", filePath)), xPos, yPos);
			break;
		case RIGHT_BREAK:
			mSpriteObject.Init(createSprite
					  (FileManager::GetFullPathToFile("\\data\\bricks\\red\\brick-right.png", filePath)), xPos, yPos);
			break;
		default:
			break;
		}
		*/
		mHp = RED_BRICK_HP;
		mSpriteObject.Init(SpriteLibrary::GetLvlSpriteCollection
						(LvlSpriteCollection::RED_BRICK_SPRITE), xPos, yPos);
	}

	else if (brickMaterial == STEEL_BRICK)
	{
		/*  in the process of developing
		switch (brickType)
		{
		case WHOLE_BRICK:
			mSpriteObject.Init(createSprite
			(FileManager::GetFullPathToFile("\\data\\bricks\\steel\\brick-whole.png", filePath)), xPos, yPos);
			break;
		case TOP_BREAK:
			mSpriteObject.Init(createSprite
			(FileManager::GetFullPathToFile("\\data\\bricks\\steel\\brick-top.png", filePath)), xPos, yPos);
			break;
		case DOWN_BREAK:
			mSpriteObject.Init(createSprite
			(FileManager::GetFullPathToFile("\\data\\bricks\\steel\\brick-down.png", filePath)), xPos, yPos);
			break;
		case LEFT_BREAK:
			mSpriteObject.Init(createSprite
			(FileManager::GetFullPathToFile("\\data\\bricks\\steel\\brick-left.png", filePath)), xPos, yPos);
			break;
		case RIGHT_BREAK:
			mSpriteObject.Init(createSprite
			(FileManager::GetFullPathToFile("\\data\\bricks\\steel\\brick-right.png", filePath)), xPos, yPos);
			break;
		default:
			break;
		}
		*/
		mHp = STEEL_BRICK_HP;
		mSpriteObject.Init(SpriteLibrary::GetLvlSpriteCollection
				(LvlSpriteCollection::STEEL_BRICK_SPRITE), xPos, yPos);
	}

	mWidth = mSpriteObject.GetWidth();
	mHeight = mSpriteObject.GetHeight();

	Actor::Init();
}

void Brick::Draw()
{
	mSpriteObject.Draw();
}

void Brick::Destroy(int dmg)
{
	mHp -= dmg;
	if (mHp < 0) mHp = 0;
}

