#pragma once

#include "TankBase.h"

class EnemyTank :
	public TankBase
{
public:
	virtual void Update(uint32_t deltaTime) override;
	void MoveTank(uint32_t deltaTime);
	void FindMovementDirection();

private:
	virtual void SetupDirections() override;
	virtual void InitSpriteObject(Vec2D defaultSpawnPoint) override;
	virtual void DestroyTank() override;
	virtual void ReceiveDmg(Bullet& bullet) override;


	long long mTimeBetweenShots = 0;
	long long mTimeChangeDirection = 0;
	
	const int FIRE_RATE = 3000;
	const int PAUSE_BEFORE_LAUNCH = 2000;
};
