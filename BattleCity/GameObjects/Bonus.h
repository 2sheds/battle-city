#pragma once

#include "Actor.h"

class PlayerTank;

class Bonus :
	public Actor
{
public:
	void Init();
	void Update(uint32_t deltaTime);
	void SpawnBonus();
	void Draw();

private:
	void UpdateBonusPosition();
	void CheckCollisionWithTank();
	void Disable();
	
	bool mIsSpawned = false;
};
