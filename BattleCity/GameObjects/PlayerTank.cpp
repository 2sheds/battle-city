#include "SpriteLibrary.h"
#include "PlayerTank.h"
#include "../BattleCity.h"

#include <iostream>

void PlayerTank::Init(Vec2D defaultSpawnPoint, TankType tankType)
{
	mCurrentLives = DEFAULT_LIVES_NUM;
	TankBase::Init(defaultSpawnPoint, tankType);
	SpriteLibrary::SpawnFX(Effects::TANK_SPAWN, mSpriteObject.GetCenterPoint());
}

void PlayerTank::SetupDirections()
{
	mDirection = Vec2D::Zero;
	mGunDirection = UP_DIR;
}

void PlayerTank::InitSpriteObject(Vec2D defaultSpawnPoint)
{
	mSpriteObject.Init(SpriteLibrary::GetTankSprites(Direction::TOP, mTankType),
					   defaultSpawnPoint.GetX(), defaultSpawnPoint.GetY());
}

void PlayerTank::Update(uint32_t deltaTime)
{
	if (mDirection != Vec2D::Zero)
	{
		mSpriteObject.MoveBy(mDirection * mVelocity * UtilsLibrary::MillisecondsToSeconds(deltaTime));
		SetupEdges();
	}

	mSpriteObject.Update(deltaTime);

	if (mBullet.mWasLaunched)
	{
		mBullet.Update(deltaTime);
	}
}

void PlayerTank::DestroyTank()
{
	mCurrentLives--;
	if (mCurrentLives <= 0)
	{
		BattleCity::SetGameState(GameState::GAME_OVER_LOST);
	}
	else
	{
		SpriteLibrary::SpawnFX(Effects::TANK_DESTROY, mSpriteObject.GetCenterPoint());
		mSpriteObject.MoveTo(DEFAUL_SPAWN_POINT);
		Actor::SetupEdges();
		mBullet.mWasLaunched = false;
		mHp = DEFAULT_HP;
		SpriteLibrary::SpawnFX(Effects::TANK_SPAWN, mSpriteObject.GetCenterPoint());

	}
}

void PlayerTank::ReceiveBonus()
{
	mTankType = static_cast<TankType>(UtilsLibrary::GetRandomNumber
				(TankType::ARMOR_TANK, TankType::FAST_TANK));
	mCurrentLives++;
}

void PlayerTank::ReceiveDmg(Bullet& bullet)
{
	mHp -= bullet.GetBulletDmg();
	if (mHp <= 0)
	{
		mHp = 0;
		
		DestroyTank();
	}
}