#pragma once
#include "Actor.h"

class Bullet;

class Phoenix :
	public Actor
{
public:
	void Init(int gameLvlWidth, int gameLvlHight);
	bool IsDestroy();
	void ReceiveDmg();
	void Update();
	void CollisionWithBullet(Bullet& bullet);
private:
	void Destroy();

	int mHp;
	int PHOENIX_HP = 1;
};
