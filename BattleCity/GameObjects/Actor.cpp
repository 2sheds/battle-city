#include "Actor.h"
#include "Utils.h"
#include <cmath>
#include <cassert>

Actor::~Actor()
{}

void Actor::Init()
{
	SetupEdges();
}

bool Actor::HasCollided(const SpriteObject& anotherSpriteObject, BoundaryEdge& edge) const
{
	if (mSpriteObject.Intersects(anotherSpriteObject))
	{
		int yMin = mSpriteObject.GetTopLeftPoint().GetY() >= 
			(anotherSpriteObject.GetTopLeftPoint().GetY()) ? 
			mSpriteObject.GetTopLeftPoint().GetY() : 
			anotherSpriteObject.GetTopLeftPoint().GetY();
		int yMax = mSpriteObject.GetBottomRightPoint().GetY() <= 
			anotherSpriteObject.GetBottomRightPoint().GetY() ? 
			mSpriteObject.GetBottomRightPoint().GetY() : 
			anotherSpriteObject.GetBottomRightPoint().GetY();

		int ySize = yMax - yMin;

		int xMin = mSpriteObject.GetTopLeftPoint().GetX() >= 
			anotherSpriteObject.GetTopLeftPoint().GetX() ? 
			mSpriteObject.GetTopLeftPoint().GetX() : 
			anotherSpriteObject.GetTopLeftPoint().GetX();
		int xMax = mSpriteObject.GetBottomRightPoint().GetX() <= 
			anotherSpriteObject.GetBottomRightPoint().GetX() ? 
			mSpriteObject.GetBottomRightPoint().GetX() : 
			anotherSpriteObject.GetBottomRightPoint().GetX();

		int xSize = xMax - xMin;

		if (xSize > ySize)
		{
			if (anotherSpriteObject.GetCenterPoint().GetY() > mSpriteObject.GetCenterPoint().GetY())
			{
				edge = mEdges[BOTTOM_EDGE];
			}
			else
			{
				edge = mEdges[TOP_EDGE];
			}
		}
		else
		{
			if (anotherSpriteObject.GetCenterPoint().GetX() < mSpriteObject.GetCenterPoint().GetX())
			{
				edge = mEdges[LEFT_EDGE];
			}
			else
			{
				edge = mEdges[RIGHT_EDGE];
			}
		}

		return true;
	}

	return false;
}

Vec2D Actor::GetCollisionOffset(const SpriteObject& anotherSpriteObject) const
{
	BoundaryEdge edge;
	Vec2D offset = Vec2D::Zero;

	if (HasCollided(anotherSpriteObject, edge))
	{
		int yMin = mSpriteObject.GetTopLeftPoint().GetY() >= 
			(anotherSpriteObject.GetTopLeftPoint().GetY()) ? 
			mSpriteObject.GetTopLeftPoint().GetY() :
			anotherSpriteObject.GetTopLeftPoint().GetY();
		int yMax = mSpriteObject.GetBottomRightPoint().GetY() <= 
			anotherSpriteObject.GetBottomRightPoint().GetY() ? 
			mSpriteObject.GetBottomRightPoint().GetY() : 
			anotherSpriteObject.GetBottomRightPoint().GetY();

		int ySize = yMax - yMin;

		int xMin = mSpriteObject.GetTopLeftPoint().GetX() >= 
			anotherSpriteObject.GetTopLeftPoint().GetX() ? 
			mSpriteObject.GetTopLeftPoint().GetX() : 
			anotherSpriteObject.GetTopLeftPoint().GetX();
		int xMax = mSpriteObject.GetBottomRightPoint().GetX() <= 
			anotherSpriteObject.GetBottomRightPoint().GetX() ? 
			mSpriteObject.GetBottomRightPoint().GetX() : 
			anotherSpriteObject.GetBottomRightPoint().GetX();

		int xSize = xMax - xMin;

		if ( edge.normal.GetY() == 0)
		{
			offset = edge.normal * (ySize + 1);
		}
		else
		{
			offset = edge.normal * (xSize + 1);
		}
	}

	return offset;
}

void Actor::MoveBy(const Vec2D& delta)
{
	mSpriteObject.MoveBy(delta);
	SetupEdges();
}

void Actor::MoveTo(const Vec2D& point)
{
	mSpriteObject.MoveTo(point);
	SetupEdges();
}

const BoundaryEdge& Actor::GetEdge(EdgeType edge) const
{
	assert(edge != NUM_EDGES);
	return mEdges[edge];
}

void Actor::SetupEdges()
{
	mEdges[TOP_EDGE].edge = { mSpriteObject.GetTopLeftPoint().GetX(), mSpriteObject.GetTopLeftPoint().GetY(), 
		mSpriteObject.GetBottomRightPoint().GetX(), mSpriteObject.GetTopLeftPoint().GetY() };
	mEdges[TOP_EDGE].normal = UP_DIR;

	mEdges[LEFT_EDGE].edge = { mSpriteObject.GetTopLeftPoint().GetX(), mSpriteObject.GetTopLeftPoint().GetY(), 
		mSpriteObject.GetTopLeftPoint().GetX(), mSpriteObject.GetBottomRightPoint().GetY() };
	mEdges[LEFT_EDGE].normal = LEFT_DIR;

	mEdges[BOTTOM_EDGE].edge = { mSpriteObject.GetTopLeftPoint().GetX(), mSpriteObject.GetBottomRightPoint().GetY(), 
		mSpriteObject.GetBottomRightPoint().GetX(), mSpriteObject.GetBottomRightPoint().GetY() };
	mEdges[BOTTOM_EDGE].normal = DOWN_DIR;

	mEdges[RIGHT_EDGE].edge = { mSpriteObject.GetBottomRightPoint().GetX(), mSpriteObject.GetTopLeftPoint().GetY(), 
		mSpriteObject.GetBottomRightPoint().GetX(), mSpriteObject.GetBottomRightPoint().GetY() };
	mEdges[RIGHT_EDGE].normal = RIGHT_DIR;
}

void Actor::MakeFlushWithEdge(const BoundaryEdge& edge, Vec2D& pointOnEdge, bool limitToEdge)
{
	if (edge.normal == DOWN_DIR)
	{
		mSpriteObject.MoveTo(Vec2D(mSpriteObject.GetTopLeftPoint().GetX(), 
			edge.edge.GetP0().GetY() + edge.normal.GetY()));
	}
	else if (edge.normal == UP_DIR)
	{
		mSpriteObject.MoveTo(Vec2D(mSpriteObject.GetTopLeftPoint().GetX(), 
			edge.edge.GetP0().GetY() - mSpriteObject.GetHeight()));
	}
	else if (edge.normal == RIGHT_DIR)
	{
		mSpriteObject.MoveTo(Vec2D(edge.edge.GetP0().GetX() + 
			edge.normal.GetX(), mSpriteObject.GetTopLeftPoint().GetY()));
	}
	else if (edge.normal == LEFT_DIR)
	{
		mSpriteObject.MoveTo(Vec2D(edge.edge.GetP0().GetX() - 
			mSpriteObject.GetSOWidth(), mSpriteObject.GetSOYpos()));
	}
}

