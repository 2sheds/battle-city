#pragma once

#include "Actor.h"

enum BulletType {
	TIER_ONE_BULLET = 0, 
	TIER_TWO_BULLET,
	TIER_THREE_BULLET,
	TIER_FOUR_BULLET
};

enum BulletSpeed {
	SLOW_BULLET = 200,
	NORMAL_BULLET = 250,
	FAST_BULLET = 300,
	NUM_BULLET
};


class TankBase;

class Bullet :
	public Actor
{
public:
	
	void Init(TankBase& shooter);
	void Update(uint32_t deltaTime);
	void Draw();
	void DestroyBullet();
	inline BulletType GetBulletType() { return mBulletType; }
	inline int GetBulletDmg() { return mDmg; }
	bool mWasLaunched = false;

private:
	void SetupBulletDirection(TankBase& shooter);

	int mXpos, mYpos;
	int mWidth, mHeight;
	int mDmg;
	int mVelocity;
	Vec2D mDirection;
	BulletType mBulletType;
	
	const int DEFAULT_BULLET_VELOCITY = 250;
	const int DEFAULT_DMG = 1;
	const int INCREASED_DMG = 2;
};

