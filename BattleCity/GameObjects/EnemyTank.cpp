#include "EnemyTank.h"
#include "SpriteLibrary.h"
#include "../BattleCity.h"


void EnemyTank::SetupDirections()
{
	mDirection = Vec2D::Zero;
	mGunDirection = DOWN_DIR;
}

void EnemyTank::InitSpriteObject(Vec2D defaultSpawnPoint)
{
	mSpriteObject.Init(
		SpriteLibrary::GetTankSprites(Direction::DOWN, mTankType),
					   defaultSpawnPoint.GetX(), defaultSpawnPoint.GetY());
}

void EnemyTank::Update(uint32_t deltaTime)
{
	mTimeBetweenShots += deltaTime;

	if (mTimeBetweenShots > FIRE_RATE && !mBullet.mWasLaunched)
	{
		Shot();
		mTimeBetweenShots = 0;
	}

	mBullet.Update(deltaTime);

	MoveTank(deltaTime);
	if (mDirection != Vec2D::Zero)
	{
		mSpriteObject.MoveBy(mDirection * mVelocity * UtilsLibrary::MillisecondsToSeconds(deltaTime));
		SetupEdges();
	}

	mSpriteObject.Update(deltaTime);
}

void EnemyTank::DestroyTank()
{
	mIsAlife = false;
	GameLevel::Singleton().mEnemyTanksToKill--;
	SpriteLibrary::SpawnFX(Effects::TANK_DESTROY, mSpriteObject.GetCenterPoint());
}

void EnemyTank::ReceiveDmg(Bullet& bullet)
{
	mHp -= bullet.GetBulletDmg();
	if (mHp <= 0)
	{
		mHp = 0;
		DestroyTank();
	}
}

void EnemyTank::MoveTank(uint32_t deltaTime)
{
	mTimeChangeDirection += deltaTime;
	if (mTimeChangeDirection > PAUSE_BEFORE_LAUNCH)
	{
		FindMovementDirection();
		mTimeChangeDirection = 0;
	}
}

void EnemyTank::FindMovementDirection()
{
	Direction randomDirection = static_cast<Direction>(UtilsLibrary::GetRandomNumber(3, 1));

	switch (randomDirection)
	{
	case TOP:
		SetMovementDirection(Direction::TOP);
		break;
	case DOWN:
		SetMovementDirection(Direction::DOWN);
		break;
	case LEFT:
		SetMovementDirection(Direction::LEFT);
		break;
	case RIGHT:
		SetMovementDirection(Direction::RIGHT);
		break;
	default:
		break;
	}
}
