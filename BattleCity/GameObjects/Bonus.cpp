#include "Bonus.h"
#include "GameLevel.h"
#include "FileManager.h"
#include "SpriteLibrary.h"
#include "Utils.h"
#include <string>

void Bonus::Init()
{
	std::string filePath;
	mSpriteObject.Init(SpriteLibrary::GetLvlSpriteCollection
					   (LvlSpriteCollection::BONUS_SPRITE), 0, 0);
}

void Bonus::UpdateBonusPosition()
{
	int possibilytyXpos = UtilsLibrary::GetRandomNumber(LVL_GRID_WIDTH) * CELL_SIDE_SIZE;
	int possibilytyYpos = UtilsLibrary::GetRandomNumber(LVL_GRID_HEIGHT) * CELL_SIDE_SIZE;


	if (GameLevel::Singleton().IsCellFree(Vec2D(possibilytyXpos, possibilytyYpos)))
	{
		mSpriteObject.ClearPoints();
		mSpriteObject.SetupPosition(Vec2D(possibilytyXpos, possibilytyYpos));
	}
	else
	{
		UpdateBonusPosition();
	}
}

void Bonus::CheckCollisionWithTank()
{
	if (mSpriteObject.Intersects(GameLevel::Singleton().mPlayerTank.mSpriteObject))
	{
		GameLevel::Singleton().mPlayerTank.ReceiveBonus();
		Disable();
	}
}

void Bonus::SpawnBonus()
{
	UpdateBonusPosition();
	
	mIsSpawned = true;
}

void Bonus::Update(uint32_t deltaTime)
{
	if (mIsSpawned)
	{
		CheckCollisionWithTank();
	}
}

void Bonus::Disable()
{
	SpriteLibrary::SpawnFX(Effects::BONUS_SPAWN, mSpriteObject.GetCenterPoint());
	mIsSpawned = false;
}


void Bonus::Draw()
{
	if (mIsSpawned)
	{
		mSpriteObject.Draw();
	}
}