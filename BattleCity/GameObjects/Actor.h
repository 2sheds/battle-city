#pragma once
#include "../Framework.h"
#include "AARectangle.h"
#include "BoundaryEdges.h"
#include "SpriteObject.h"
#include "Vec2D.h"


class Actor
{
public:
	virtual ~Actor();

	virtual void Init();

	bool HasCollided(const SpriteObject& spriteObject, BoundaryEdge& edge) const;
	Vec2D GetCollisionOffset(const SpriteObject& anotherSpriteObject) const;

	void MoveBy(const Vec2D& delta);
	void MoveTo(const Vec2D& points);

	const BoundaryEdge& GetEdge(EdgeType edge) const;

	void MakeFlushWithEdge(const BoundaryEdge& edge, Vec2D& pointOnEdge, bool limitToEdge);
	void SetupEdges();

	SpriteObject mSpriteObject;

private:
	BoundaryEdge mEdges[NUM_EDGES];
};

